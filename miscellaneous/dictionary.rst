***************************
Frances & Family Dictionary
***************************

The Alpha
    Nickname for :doc:`Foxy </biographies/foxy>` as given by the community after he showed very alpha behavior.
    He is the most aloof kitten. He liked to stay off on his own away from the litter.
    He wasn't one for cuddling.
    He was the last one to bond with his humans and was very standoffish and solitary until his neutering.
    Later he became a fierce hunter, being the first kitten to capture the feathered wand toy.
    He then ran off with it into a cat cave and growled at his dad when he tried to take it back to continue playing.
    He even caught a fly live on the Amazon live show, Cool Cat Stuff.

Amazon Live
    Amazon’s live streaming platform where people can sell products they use
    and where the family does Cool Cat Stuff, their product review show.

The Babies
    Nickname for the litter of kittens.

The Banana
    A catnip stuffed banana by the company Yeowww! that is a beloved toy for the cats.
    (:doc:`Donda </biographies/donda>` loved it so much he stole it right from his mom the first time he saw it.)

Bub
    General term of affection for the cats by Chris, especially :doc:`Oliver </biographies/donda>` who is the #1 bub.

Dondo
    Nickname for :doc:`Donda </biographies/donda>` from ``Also_Kiki``, adopted by the rest of the community.

Cat Narnia
    Nickname for the hole in the fence in the backyard that Frances climbed through due to its magical nature.

Coffee & Cat Stuff
    The original and main cat product show starring Frances & Family
    and hosted by their dad that’s broadcast on Amazon Live.
    It showcases all the real products the cats use as they use them.

Circus Peanut
    Dad’s nickname for :doc:`Snoo </biographies/snoo>`,
    arising from her natural aptitude from early kittenhood for acrobatic feats
    and impressive circus tricks including “the tightrope”, climbing the ladder,
    jumping to the dresser, and balancing atop the scratching post.

EpopE
    Nickname for :doc:`Epona </biographies/epona>` from ``Also_Kiki``, adopted further among the community.

Epoxy
    Nickname for the combination of the twins :doc:`Epona </biographies/epona>` & :doc:`Foxy </biographies/foxy>`
    or for when the identity of one of the twins is not easily detected
    (which mostly happened when they were small kittens).

F&F
    Shorthand for “Frances & Family”.

The Group Poop
    The nickname given to the time 2 of the kittens pooped at the same time in the same litter box side by side,
    facing opposite directions to watch each others’ backs.

Kibblemania
    The act of one of the kittens spilling or plunging into a bowl of kibble to chase
    and bat them around the room which draws the participation of all
    the kittens leading to a frenzy of kibble and zoomies.
    A much loved event by community viewers, but greatly loathed by their father.

Litter Robot
    The robotic litter box used by the cats.
    The first one was generously donated by community member ``Socatastic``
    and it led to a full out adoption of Litter Robots as the main litter box system used by the cats.
    “The whole house runs on 2 Litter Robots” as their dad says.

Little Bear
    Dad’s nickname for :doc:`Epona </biographies/epona>`, often used in the context of the phrase “my little bear”.
    Frequently accompanied with or replied to using the bear emoji 🐻.

The Milkbar
    F&F term for the kitten’s nursing from momma.

Momma
    Nickname of :doc:`Frances </biographies/frances>`, mother of the kittens.

Momu
    Nickname of :doc:`Frances </biographies/frances>` from ``Also_Kiki`` adaptation of “Momma”,
    adopted by the rest of the community.

Nom a noms
    Eating and or food itself.

The Nursery
    The room dedicated to the cats, where they slept safely at night as kittens,
    and where they primarily spent their time as small kittens.

Olivia
    The cat’s sister & best canine friend.
    Olivia is a brown lab and often seen wrestling with Oliver or nuzzling the kittens during their morning greetings.
    Olivia’s favorite thing in the world is her “woobie”, a stuffed gingerbread man originally gifted
    to the kittens and then claimed as her own most prized possession.
    The original woobie died a sad death from too much love and has been replaced with a lookalike or two since.
    The original was a generous gift from community member ``Ragpicker``.

Pawsie
    An animal paw.

Princess Snoo
    Nickname of :doc:`Snoo </biographies/snoo>`, because she is a princess and the one and only princess of the group.
    It also matches her majestic nature.
    She’s the adorable baby of the bunch and she knows it.
    (And she even has a princess dress from Aunt ``CJ``.
    She does not wear it often so as not to flaunt her royal status in front of her brothers.)

The Rainbow
    :doc:`Frances </biographies/frances>`’s first and favorite toy, a catnip stuffed rainbow by the company Yeowww!

RPAN
    Reddit’s live streaming app on which Frances & Family are broadcast since :doc:`Frances </biographies/frances>`
    began visiting the yard and the app on which the kittens birth was streamed live.

The Twins
    Another nickname for :doc:`Foxy </biographies/foxy>` & :doc:`Epona </biographies/epona>`,
    the twin middle brown tabby kittens.

Tight rope
    :doc:`Snoo </biographies/snoo>`’s circus trick of walking along the narrow wall handstand ladder.

Sing
    Shorthand nickname for ``SingTheDamnSong`` the screen name of cat dad Chris.

SingTheDamnSong
    Screenname for the cat’s dad, Chris. Used on Reddit & Discord.

Snonda
    Shorthand for the combination of :doc:`Snoo </biographies/snoo>` and :doc:`Donda </biographies/donda>`.
    Both are the most inquisitive and outgoing kittens so they often are seen together leading the hijinks.

The Spaceship
    Nickname for the Litter Robot often used by the community or passersby as the robot looks vaguely like a spaceship.

The Studio
    The space where Chris & the cats do their Amazon Live shows.

Stretchy-Stretch
    A cat stretch.

(Cat name) Vision
    The term for whenever a cat is able to be flipped over and laid on their back for belly rubs.
    For example “Epona Vision”, is when :doc:`Epona </biographies/epona>` does this, he’s the originator of this pose.

Woobie
    A comfort item that’s each animal's favorite object.
    :doc:`Frances </biographies/frances>`’ woobie is the stuffed rainbow,
    :doc:`Donda </biographies/donda>`’s woobie is the banana, etc.
