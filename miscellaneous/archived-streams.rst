**********************************
Frances & Family Archived Streams
**********************************
*Titles with \* mean the video has some audio issues (TODO: fix these)*

.. csv-table::
    :file: ../assets/archived-streams.csv
    :header-rows: 1
    :class: archive compact
    :encoding: utf-8