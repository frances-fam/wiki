{ pkgs ? import <nixpkgs> {} }:
let
  my-python = pkgs.python3;
  python-with-my-packages = my-python.withPackages (p: with p; [
    sphinx_rtd_theme
  ]);
in
pkgs.mkShell {
  buildInputs = [
    python-with-my-packages
    pkgs.gnumake
  ];
  shellHook = ''
    PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}
    # maybe set more env-vars
  '';
}
