*********************************************
Snoo, 9 months, Only Female & Youngest Kitten
*********************************************

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - August 27, 2021 in Philadelphia, PA
    * - Aliases & Nicknames
      - Princess Snoo, Circus Peanut
    * - Breed
      - Multi-colored domestic shorthair tabico (gray, black, orange & white)

Physical Description
====================

Light gold eyes, multi-colored coat on a white base
with patches of gray, black, and orange including one completely orange ear.
A speckled pink and black nose. 2-toned pink and black mouth.
Beautiful white boots just like her momma.
And a tail with a tiny white tip on the end of it.

Name Origin
===========

After Reddit's company mascot called a "Snoo", since she was born live on Reddit.

Likes
=====

Being daddy's Princess, circus tricks, being momma's mini me, kibblemania, morning time spa massages,
eating much more than her body weight would suggest she could,
giving 'tude when appropriate, the giant stuffed carrot toy.

Biography
============

While Foxy is very Alpha, Snoo has taken it upon herself to be the soft leader of the kittens.
The mini momma if you will.
She’s often found helping her brothers with their grooming, an interest she’s shown since early kittenhood.
Snoo is wise beyond her years in that way.
Snoo is fastidious in her cleaning and definitely the best groomer of the bunch.
She keeps her white boots spotless.
She also helps her brother groom when they are feeling lazy.
She even helps her mom groom!

Snoo is possibly the smartest of all the kittens figuring out how to climb the ladder and jump to the dresser first,
where the laser toy’s beam came from, and how to get up into the wall hammocks first,
and she also appeared to once say “yeah” in response to the question “you hungy?”
Genius confirmed.
