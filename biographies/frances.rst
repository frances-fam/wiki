*************************************
Frances, 1-1 ½, Mother of the Kittens
*************************************

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - c\. 2020 in Philadelphia, PA ("Gotcha day" - August 25, 2021)
    * - Aliases & Nicknames
      - Momma/Mama, Momu, happy little potato
    * - Significant Other
      - Oliver the orange tabby
    * - Breed
      - Unknown shorthair


Physical Description
====================

Frances has a light brown color coat with fine speckles of darker browns, tans, and a touch of black throughout.
She is known for her trademark white booted paws which is a color she also features on her chest
and in a little splotch on her face and nose.
She has bright yellow eyes and a two color nose:mostly dark red with a corner of light pink.
Her toe beans are noted for being adorably speckled with several colors.
Frances is a full figured lady after being spayed and compensatory eating due to her background
of being a formerly deprived stray but she is working on slimming down and is fabulous nonetheless.

Likes
=====

Her first ever and favorite toy, the Yeowww!
Rainbow catnip toy (commonly known as "the rainbow" or "mama’s rainbow").
Snoo, her favorite kitten.
Oliver, her significant other and the resident handsome orange tabby.
Morning lap time with dad. Wrestling matches with beau Oliver.
The nursery chair.

Biography
=========

The little lady who started it all!
Frances was a stray cat on the streets of Philadelphia when one day in the spring of 2021 she decided to peek
through a hole in a fence from an alleyway.
That’s when her Disney movie life really begins, as she was lucky enough to find the fence hole
of some very kind people who help the neighborhood strays out with some meals.
Eventually her beauty intrigued them so much that they decided to try to befriend her.
Frances however was very scared of people.
Many overtures by the kind people were rebuffed,
she was too skittish to trust yet but she kept coming back to the yard.
Soon it was clear Frances wasn't alone, she appeared pregnant.
A few months passed and Frances kept returning while allowing the people to get closer on those visits.
\*once even pet her, then a few pets more.
She continued to return for meals faithfully and slowly made cautious progress befriending the humans.
Frances lived a rough life out on the streets though often fighting off racoons and opossums to get to her meals.
She grew more visibly pregnant by the day.
Then on August 25th, 2021 Frances returned to the yard with blood on her ear.
The humans were very concerned and decided to take a chance and bring her inside.
