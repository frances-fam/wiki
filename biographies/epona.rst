****************************************
Epona, 9 months, Twin Middle Male Kitten
****************************************

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - August 27, 2021 in Philadelphia, PA
    * - Aliases & Nicknames
      - Epona Bear, Little Bear, EpopE
    * - Breed
      - Brown domestic shorthair tabby

Physical Description
====================

Epona is one of the two twin brown tabby kittens.
He's the second biggest, behind Donda.
He's differentiated from his twin Foxy by being slightly bigger and having a wider face with less pronounced ears.
He's often said to resemble a little bear, which is where his nickname comes from.

Name Origin
===========

Named after Reddit user/Discord mod ``Epona Mom`` who helped give advice to dad
while was helping Frances give birth to the kittens live on a Reddit stream.

Likes
=====

Climbing the curtains, twinning with Foxy, Epona Vision, hanging upside down from dad,
being a shoulder cat, kibblemania.

Biography
============

Epona is chill.
Jimmy Buffet smoking reefer with Snoop Dogg on an iceberg chill.
He’s like the Big Lebowski of cats.
Except he’s also the boy you can take home to meet mom too.
He’s very agreeably natured and is considered the best behaving kitten.
Epona never lets his gas tank get below a quarter full.
Epona has a Roth IRA.
Epona never just lets the dishes soak until tomorrow.
