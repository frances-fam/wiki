*****************************************************
Oliver, 4 years, Frances' Partner & Kitten Stepfather
*****************************************************

Parental status is disputed by a contingent of family supporters led by The Jeeley who believes Oliver once escaped
to the yard and fathered the kittens biologically.
This account of events has been denied by the cat's human father Chris.
This is a running joke in the community however.

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - c\. 2018 in Philadelphia, PA
    * - Aliases & Nicknames
      - Ollie, Bub, My (his dad's) #1 Bub
    * - Significant Other
      - Frances the reformed stray
    * - Breed
      - Orange domestic shorthair tabby


Physical Description
====================

Oliver is a medium-shade orange tabby with light caramel color eyes.
He is a very muscular cat and people have joked about his buffness.

Likes
=====

Frances, wrestling with Frances, wrestling with Olivia, cuddling with his kittens
