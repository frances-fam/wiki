***********************************
Donda, 9 months, Eldest Male Kitten
***********************************

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - August 27, 2021 in Philadelphia, PA
    * - Aliases & Nicknames
      - Dondo, Donda Donda Donda (often said in frustration), little snow leopard
    * - Breed
      - Gray domestic shorthair tabby

Physical Description
====================

Donda is the only gray colored kitten.
He is also the biggest kitten and has been the largest since birth.
Donda's coat has very pronounced spotted patterns on his belly (thus his "snow leopard" nickname).
Donda's eyes match his coat and are also gray making him the only kitten without yellow or gold eye color.
His most notable feature is probably his striking white colored eyeliner.

Likes
=====

The banana catnip toy, stealing the rainbow toy from his mom, being a maniac, kibblemania, manspreading, Olivia the dog.

Dislikes
========

Recovery collar cones (*"get it off me, get it off me, ahhhhhh!"*)

Biography
============

Donda is a ground breaker.
He was the first kitten to be born, the first to leave his birthplace cat bed to explore,
the first to climb the ladder and scare the heck out of his dad.
