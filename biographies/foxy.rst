***************************************
Foxy, 9 months, Twin Middle Male Kitten
***************************************

General information
===================

.. list-table::
    :stub-columns: 1

    * - Born
      - August 27, 2021 in Philadelphia, PA
    * - Aliases & Nicknames
      - The Alpha
    * - Breed
      - Brown domestic shorthair tabby

Physical Description
====================

Foxy is the second of the two twin brown tabby kittens.
He's slightly smaller in size and weight than his twin brother Epona.
He's also differentiated from his twin by being his signature extra long ears
(which grew before the rest of his head as a kitten) and having a slightly slimmer face.

Name Origin
===========

Named after Reddit user ``FoxyLady``, another person who was helpful in giving advice to dad while was helping
Frances give birth to the kittens live on a Reddit stream.

Likes
=====

Being dominant, doing his own thing, hunting wand toy feathers, hunting, killing, and eating flies,
twinning with Epona, hanging out on the top of the kitchen cabinets near the ceiling, kibblemania.
